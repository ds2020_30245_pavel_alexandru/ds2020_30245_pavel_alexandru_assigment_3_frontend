import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../api/doctor-api";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import UpdatePatientForm from "./updatepatient-form";
import UpdateMedicationForm from "./updatemedication-form";


const columns = [

    {
        Header: 'Id',
        accessor: 'id',
        show: false,
    },

    {
        Header: 'Medication Name',
        accessor: 'name',
    },
    {
        Header: 'Side Effects',
        accessor: 'sideEffects',
    },
    {
        Header: 'Dosage',
        accessor: 'dosage',
    },
    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "red", color:"#fefefe"}}  onClick={()=>{window.thisViewNew.deleteMedicationId(props.original.id)}}>Delete</button>

            )
        },
        width: 100,
        maxWidth: 100,
        minWidth: 100,
    },
    {
        Cell: props=>{
            return(
                <button style={{backgroundColor: "green", color:"#fefefe"}} onClick={()=>{window.thisViewNew.toggleForm4(props.original.id,props.original.name,props.original.sideEffects,props.original.dosage)}}>Update</button>
            )
        },
        width: 100,
        maxWidth: 100,
        minWidth: 100
    }
];

const filters1 = [
    {
        accessor: 'name'
    }
];

class MedicationsTable extends React.Component {

    constructor(props) {
        super(props);

        this.toggleForm4=this.toggleForm4.bind(this);
        this.reloadHandlerTableMedication=this.props.reloadHandlerTableMedication;

        this.state = {
            tableMedicationData: this.props.tableMedicationData,
            updatedIdMeds: [],
            informationMeds: [],
            collapseForm4: false,
            selected4: false
        };
        window.thisViewNew=this;
    }

    toggleForm4(id,name,sideEffects,dosage)
    {
        let medication = {
            name:name,
            sideEffects: sideEffects,
            dosage: dosage,
        };

        this.state.updatedIdMeds=id;
        this.state.informationMeds=medication;

        this.setState({selected4: !this.state.selected4});
    }

    deleteMedicationId(id) {
        return API_USERS.deleteMedication(id,(result, status, err) => {

            if (result !== null && (status === 200 || status ===204 || status===202))
            {
                console.log("Deleted medication with id:",id);

                this.reloadHandlerTableMedication();

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    render() {
        return (
            <div>
            <Table
                data={this.state.tableMedicationData}
                columns={columns}
                search={filters1}
                pageSize={5}
            />
                <Modal isOpen={this.state.selected4} toggle={this.toggleForm4}
                       className={this.props.className} size="lg" >
                    <ModalHeader toggle={this.toggleForm4}> Edit Medication Information: </ModalHeader>
                    <ModalBody>
                        <UpdateMedicationForm informationMeds = {this.state.informationMeds} updatedIdMeds = {this.state.updatedIdMeds} reloadForUpdateMeds = {this.reloadHandlerTableMedication}/>
                    </ModalBody>
                </Modal>
            </div>

        )
    }

}

export default MedicationsTable;