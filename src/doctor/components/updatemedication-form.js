import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class UpdateMedicationForm extends React.Component {


    constructor(props) {

        super(props);
        this.toggleForm4 = this.toggleForm4.bind(this);
        this.reloadForUpdateMeds = this.props.reloadForUpdateMeds;

        this.state = {

            medsInfo: this.props.informationMeds,
            medsId: this.props.updatedIdMeds,

            errorStatus: 0,
            error: null,
            link_id: '',

            formControls: {
                name: {
                    value: this.props.informationMeds.name,
                    touched: false,
                },
                sideEffects: {
                    value: this.props.informationMeds.sideEffects,
                    touched: false,
                },
                dosage: {
                    value: this.props.informationMeds.dosage,
                    touched: false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm4() {
        this.setState({collapseForm4: !this.state.collapseForm4});
    }

    updateMedication(id,medication) {
        return API_USERS.updateMedication(id,medication, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.reloadForUpdateMeds();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medication = {
            name: this.state.formControls.name.value,
            sideEffects: this.state.formControls.sideEffects.value,
            dosage: this.state.formControls.dosage.value,
        };

        this.updateMedication(this.state.medsId,medication);
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;

        this.setState({
            formControls: updatedControls,
        });

    };

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Medication Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.medsInfo.name}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='sideEffects'>
                    <Label for='sideEffectsField'> Known Side Effects: </Label>
                    <Input name='sideEffects' id='sideEffectsField' placeholder={this.state.formControls.sideEffects.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.medsInfo.sideEffects}
                           touched={this.state.formControls.sideEffects.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='dosage'>
                    <Label for='dosageField'> Dosage: </Label>
                    <Input name='dosage' id='dosageField' placeholder={this.state.formControls.dosage.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.medsInfo.dosage}
                           touched={this.state.formControls.dosage.touched? 1 : 0}
                           required
                    />
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default UpdateMedicationForm;
