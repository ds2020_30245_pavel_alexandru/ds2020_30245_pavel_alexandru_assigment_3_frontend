import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class UpdateCaregiverForm extends React.Component {


    constructor(props) {
        super(props);

        this.toggleFormCaregiver = this.toggleFormCaregiver.bind(this);
        this.reloadForUpdate = this.props.reloadForUpdate;

        this.state = {

            caregiverInfo: this.props.informationCaregiver,
            caregiverId: this.props.updatedIdCaregiver,

            errorStatus: 0,
            error: null,
            link_id: '',

            formControls: {
                name: {
                    value: this.props.informationCaregiver.name,
                    touched: false,
                },
                dob: {
                    value: this.props.informationCaregiver.dob,
                    touched: false,
                },
                gender: {
                    value: this.props.informationCaregiver.gender,
                    touched: false,
                },
                address: {
                    value: this.props.informationCaregiver.address,
                    touched: false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleFormCaregiver() {
        this.setState({collapseForm2: !this.state.collapseForm2});
    }

    updateCaregiver(id,caregiver) {
        return API_USERS.updateCaregiver(id,caregiver, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                this.reloadForUpdate();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let caregiver = {
            name: this.state.formControls.name.value,
            dob: this.state.formControls.dob.value,
            gender: this.state.formControls.gender.value,
            address: this.state.formControls.address.value,
            patients: ""
        };

        this.updateCaregiver(this.state.caregiverId,caregiver);
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;


        this.setState({
            formControls: updatedControls,
        });

    };

    render() {
        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.caregiverInfo.name}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='dob'>
                    <Label for='dobField'> Date of Birth: </Label>
                    <Input name='dob' id='dobField' placeholder={this.state.formControls.dob.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.caregiverInfo.dob}
                           touched={this.state.formControls.dob.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.caregiverInfo.gender}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.caregiverInfo.address}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default UpdateCaregiverForm;
