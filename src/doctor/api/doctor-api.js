import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";




const endpoint = {
    doctor: '/doctor/',
    patients: '/patient/',
    caregivers: '/caregiver/',
    medications: '/medication/',
    user: '/user/',
    medicationplan: '/medicationPlan/'
};

function getDoctor(id, callback) {
    let request = new Request(HOST.backend_api  + endpoint.doctor + id, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function addMedToPlan(medPlanId, name, selected, callback) {
    let request = new Request(HOST.backend_api  + endpoint.medicationplan + "update/" + medPlanId + "/" + name + "/" + selected, {
        method: 'PUT',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatients(callback) {
    let request = new Request(HOST.backend_api  + endpoint.patients, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api  + endpoint.caregivers, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedications(callback) {
    let request = new Request(HOST.backend_api  + endpoint.medications, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}
function addPatientToCaregiver(id,patientId,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.caregivers + "update/" + id + "/" + patientId, {
        method: 'PUT',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function removePatientFromCaregiver(id,patientId,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.caregivers + "update/" + id + "/" + patientId, {
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function addPatient(params,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.patients , {
        method: 'POST',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function addCaregiver(params,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.caregivers, {
        method: 'POST',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function addMedicationPlan(medPlan, callback)
{
    console.log(medPlan);
    let request = new Request(HOST.backend_api  + endpoint.medicationplan, {
        method: 'POST',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(medPlan)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function addUser(params,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.user , {
        method: 'POST',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function addMedication(params,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.medications , {
        method: 'POST',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function deletePatient(id, callback)
{
    let request = new Request(HOST.backend_api  + endpoint.patients + id, {
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(id, callback)
{
    let request = new Request(HOST.backend_api  + endpoint.caregivers + id, {
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteUser(id, callback)
{
    let request = new Request(HOST.backend_api  + endpoint.user + "byAccountId/" + id, {
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function updatePatient(id,patient,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.patients + id , {
        method: 'PUT',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(patient)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(id,caregiver,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.caregivers + id , {
        method: 'PUT',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(caregiver)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteMedication(id, callback)
{
    let request = new Request(HOST.backend_api  + endpoint.medications + id, {
        method: 'DELETE',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientsByCaregiver(id, callback)
{
    let request = new Request(HOST.backend_api  + endpoint.caregivers + "patients/" + id, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function updateMedication(id,medication,callback)
{
    let request = new Request(HOST.backend_api  + endpoint.medications + id , {
        method: 'PUT',
        headers : {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(medication)
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getDoctor,
    getPatients,
    getCaregivers,
    getMedications,
    addPatient,
    addUser,
    deletePatient,
    deleteUser,
    updatePatient,
    addMedication,
    deleteMedication,
    updateMedication,
    addCaregiver,
    deleteCaregiver,
    updateCaregiver,
    getPatientsByCaregiver,
    addPatientToCaregiver,
    removePatientFromCaregiver,
    addMedicationPlan,
    addMedToPlan
};
