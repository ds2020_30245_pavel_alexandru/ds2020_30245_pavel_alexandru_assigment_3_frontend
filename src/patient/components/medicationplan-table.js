import React from "react";
import Table from "../../commons/tables/table";
import * as Moment from "moment";


const columns = [
    {
        Header: 'Medications',
        accessor: 'meds',
    },
    {
        Header: 'Administration Intervals',
        accessor: 'medsTime',
    },
    {
        Header: 'Medication Plan Start Date',
        id: 'startDate',
        accessor: d => {
            return Moment(d.startDate)
                .local()
                .format("DD-MM-YYYY")
        }
    },
    {
        Header: 'Medication Plan End Date',
        id: 'endDate',
        accessor: d => {
            return Moment(d.endDate)
                .local()
                .format("DD-MM-YYYY")
        }
    }
];

const filters1 = [
    {
        accessor: 'startDate'
    }
];

class MedicationplanTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters1}
                pageSize={5}
            />
        )
    }
}

export default MedicationplanTable;