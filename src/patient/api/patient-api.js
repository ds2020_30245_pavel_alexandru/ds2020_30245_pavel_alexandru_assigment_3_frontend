import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient/',
    medications: '/medicationplans/'
};

function getPatient(id, callback) {
    let request = new Request(HOST.backend_api  + endpoint.patient + id, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicalPlans(id, callback)
{
    let request = new Request(HOST.backend_api  + endpoint.patient + endpoint.medications + id, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatient,
    getMedicalPlans
};
