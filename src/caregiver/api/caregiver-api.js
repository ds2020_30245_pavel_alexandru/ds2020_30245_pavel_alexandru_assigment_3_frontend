import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver/',
    patients: '/patients/'
};

function getPatients(id, callback) {
    let request = new Request(HOST.backend_api  + endpoint.caregiver + endpoint.patients + id, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatients
};
